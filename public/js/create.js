$( document ).ready(function() {
    var quill = new Quill('#editor', {
        theme: 'snow'
    });

    $('#button').click(function (e) {
        delta = quill.getText();
        console.log(delta);
        $.ajax({
            type: 'POST',
            url : '/publish',
            data: {header: null, text: delta}
        }).done(function (data) {
            console.log(data);
            if (data.status == true){
                $(location).attr('href', '/')
            }
        });
    })
});