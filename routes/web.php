<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function (\Illuminate\Http\Request $request) use ($router) {
    if($request->cookie('api_token')){
        return view('main');
    }
    if(!$request->cookie('api_token')){
        $res['success'] = true;
        $res['result'] = 'Hello world with lumen';
        return response($res);
    }
});

$router->get('/auth', function () use ($router){
   return view('auth');
});
$router->post('/login', 'LoginController@index');
$router->post('/register', 'UserController@register');
$router->get('/posts', 'PostsController@getPosts');
$router->post('/publish', 'PostsController@publish');
$router->get('/create', function (){
    return view('create');
});
$router->get('post/{id}', 'PostsController@getPostPage');
