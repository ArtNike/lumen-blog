<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.01.18
 * Time: 2:33
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author_id', 'header', 'text', 'date',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $table = 'posts';

    public $timestamps = false;
}