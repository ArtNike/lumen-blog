<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.01.18
 * Time: 2:35
 */

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class PostsController
{
    public function getPosts(){
        $posts = Post::all();
        return $posts;
    }

    public function publish(Request $request){
        $api_token = $_COOKIE['api_token'];
        $user = User::where('api_token', $api_token)->first();
        $header = '';
        if($request->header){
            $header = $request->header;
        }
        if($user){
            $post = Post::create([
                'author_id'=>$user->id,
                'header'=>$header,
                'text'=> $request->text,
                'date'=> date("Y-m-d H:i:s")
            ]);
            return response()->json(['status'=>true]);
        }
        else return response()->json(['status'=>false]);
    }

    public function getPostPage($postId){
        $post = Post::where('id', $postId)->first();
        if($post){
            return view('post', ['post'=>$post]);
        }
        else return redirect('/');
    }
}