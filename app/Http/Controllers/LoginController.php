<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 03.01.18
 * Time: 16:13
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;

class LoginController extends Controller
{
    /**
     * Index login controller
     *
     * When user success login will retrive callback as api_token
     */
    public function index(Request $request)
    {
        $hasher = app()->make('hash');

        $name = $request->input('username');
        $password = $request->input('password');
        $login = User::where('name', $name)->first();

        if ( ! $login) {
            $res['success'] = false;
            $res['message'] = 'Your email or password incorrect!';
            return response($res);
        } else {
            if ($hasher->check($password, $login->password)) {
                $api_token = sha1(time());
                $create_token = User::where('id', $login->id)->update(['api_token' => $api_token]);
                if ($create_token) {
                    $res = new \Illuminate\Http\Response(view('main'));
                    $res->success = true;
                    $res->api_token = $api_token;
                    $res->message = $login;
                    $cookie_name = "api_token";
                    setcookie($cookie_name, $api_token, time() + (86400 * 30), "/"); // 86400 = 1 day
                    return redirect('/');
                }
            } else {
                $res['success'] = true;
                $res['message'] = 'You username or password incorrect!';
                return $res;
            }
        }
    }
}